import math

import numpy as np
import pygame
from OpenGL.GL import *
from OpenGL.GLU import *
from pygame.locals import *

from computacao_grafica_trabalho_objeto_casa.cadeira_rd import CadeiraDr


flash_light_pos = [1.0, 6.0,2.0]
flash_light_dir = [0.0, 3.5, 1.0]
flash_light_color = [0.3, 1.0, 0.7]  # Luz clara com um tom esverdeado
# flash_light_color = [1.0, 1.0, 1.0]  # Luz clara com um tom esverdeado

white_light_color = [1.0, 1.0, 1.0]  # Luz branca
white_light_pos = [5.0, 10, 2.0, 2.0]

is_lighting = True


def opengl_init_ilumination():
    global is_lighting

    glEnable(GL_DEPTH_TEST)

    glLightfv(GL_LIGHT0, GL_POSITION, flash_light_pos)
    glLightfv(GL_LIGHT1, GL_AMBIENT, flash_light_color)
    glLightfv(GL_LIGHT0, GL_DIFFUSE, flash_light_color)
    glLightfv(GL_LIGHT0, GL_SPECULAR, flash_light_color)
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, flash_light_dir)
    glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 50.0)

    glLightfv(GL_LIGHT1, GL_AMBIENT, white_light_color)
    glLightfv(GL_LIGHT1, GL_DIFFUSE, white_light_color)
    glLightfv(GL_LIGHT1, GL_SPECULAR, white_light_color)

    # position the white light
    glLightfv(GL_LIGHT1, GL_POSITION, white_light_pos)

    # Enable or disable light
    if is_lighting:
        glEnable(GL_LIGHT0)
        glEnable(GL_LIGHT1)

        glPushMatrix()
        glDisable(GL_LIGHTING)
        glEnable(GL_LIGHTING)
        glPopMatrix()
    else:
        # glDisable(GL_LIGHT0)
        glDisable(GL_LIGHT1)  # Somente a luz 1 (branca) é disabilitada.]
                              # Caso "apague a luz" veja uma certa sombra da cadeira.


# ahead = true or false
def move_can(eye, target, ahead):
    # equação paramétrica
    a = np.zeros(3)
    delta = 0.1
    p0 = eye
    p1 = target
    a = p1 - p0

    if ahead:
        signal = 1
    else:
        signal = -1
    p0 = p0 + delta * a * signal
    # p1 = p1 + delta * a * signal

    # Câmera sobre o plano (X, Z)
    # p0[1] = 0

    return p0, p1

    # right = true or false


def move_target(eye, target, right):
    # translada a câmera para a origem
    t0 = target - eye

    # Ignora a componente Y
    t0[1] = 0

    # Calcula o raio do círculo
    r = math.sqrt(t0[0] * t0[0] + t0[2] * t0[2])

    # Calcula o seno e o cosseno e a tangente do ângulo que o vetor faz com o eixo X
    sin_alfa = t0[2] / r
    cos_alfa = t0[0] / r
    if cos_alfa == 0:
        if sin_alfa == 1:
            alfa = math.pi / 2
        else:
            alfa = - math.pi / 2
    else:
        tg_alfa = sin_alfa / cos_alfa

        # Calcula o arco cuja tangente é calculada no passo anterior
        alfa = np.arctan(tg_alfa)

        # Como o retorno de arctan varia somente entre -pi/2 e pi/2, testar o cosseno para
        # calcular o ângulo correto
        if cos_alfa < 0:
            alfa = alfa - math.pi

    if right:
        signal = 1
    else:
        signal = -1

    # Varia o ângulo do alvo (target)
    alfa = alfa + 0.1 * signal

    # Calcula o novo alvo (sobre o eixo Y)
    t0[0] = r * math.cos(alfa)
    t0[2] = r * math.sin(alfa)

    n_target = eye + t0

    return n_target


def main_cadeira():
    global is_lighting

    # Item E: Dados para rotacionar a câmera
    radiano = 30
    angle = 3.1415 / 2

    # UP vector angle = Pi/2 (90o)
    up_angle = 3.1415 / 2

    rot_angle = 3.1415 / 2

    pygame.init()
    display = (800, 600)
    pygame.display.set_mode(display, DOUBLEBUF | OPENGL)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    gluPerspective(45, (display[0] / display[1]), 0.1, 80.0)

    # Item C: Posicionamento da câmera no ponto (0, 8, 30)
    eye = np.zeros(3)
    eye[1] = 8
    eye[2] = 30

    # Item C: Colocando como alvo de observação o ponto (0, 0, 0)
    target = np.zeros(3)

    # Item C: Coloca o Up vector apontado para o eixo Y
    up = np.zeros(3)
    up[1] = 1

    # cadeira = CadeiraDr(10, 10, 10)
    # cadeira = CadeiraDr() # 0, 0, 0
    cadeira = CadeiraDr(-10, 0, -10)

    while True:
        # print(eye)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            elif event.type == pygame.KEYDOWN:
                # Movimentação do cubo
                if event.key == K_w:
                    cadeira.ty = cadeira.ty + 0.5
                elif event.key == K_s:
                    cadeira.ty = cadeira.ty - 0.5
                elif event.key == K_a:
                    cadeira.tx = cadeira.tx - 0.5
                elif event.key == K_d:
                    cadeira.tx = cadeira.tx + 0.5

                # Movimentação da direção da câmera
                # nos eixos X e Y (olho)
                elif event.key == K_UP:
                    target[1] = target[1] + 0.5
                elif event.key == K_DOWN:
                    target[1] = target[1] - 0.5
                elif event.key == K_RIGHT:
                    # target = move_target(eye, target, True)
                    target[0] += 0.5
                elif event.key == K_LEFT:
                    # target = move_target(eye, target, False)
                    target[0] -= 0.5

                elif event.key == K_PAGEUP:
                    # Mover para frente
                    eye, target = move_can(eye, target, True)
                elif event.key == K_PAGEDOWN:
                    # Mover para trás
                    eye, target = move_can(eye, target, False)

                elif event.key == K_t:  # Mover câmera/olho para os lados
                    eye[0] = eye[0] - 0.5
                elif event.key == K_g:
                    eye[0] = eye[0] + 0.5

                # Movimentação da parte de cima da câmera (UP vector)
                elif event.key == K_PERIOD:
                    up_angle += 0.05
                    up[1] = math.sin(up_angle)
                    up[0] = math.cos(up_angle)
                elif event.key == K_COMMA:
                    up_angle -= 0.05
                    up[1] = math.sin(up_angle)
                    up[0] = math.cos(up_angle)

                # Wireframe
                elif event.key == K_SPACE:
                    cadeira.wireframe = not cadeira.wireframe

                # Girar camera
                elif event.key == K_i:
                    angle += 0.3
                    eye[0] = math.sin(angle) * radiano
                    eye[2] = math.cos(angle) * radiano
                elif event.key == K_o:
                    angle -= 0.4
                    eye[0] = math.sin(angle) * radiano
                    eye[2] = math.cos(angle) * radiano

                elif event.key == K_0:
                    is_lighting = True
                elif event.key == K_1:
                    is_lighting = False

            elif event.type == pygame.MOUSEBUTTONDOWN:
                eye_dist = math.sqrt(eye[0] * eye[0] + eye[2] * eye[2])

                if event.button == 4:
                    rot_angle += 0.04
                    eye[2] = eye_dist * math.sin(rot_angle)
                    eye[0] = eye_dist * math.cos(rot_angle)

                if event.button == 5:
                    rot_angle -= 0.04
                    eye[2] = eye_dist * math.sin(rot_angle)
                    eye[0] = eye_dist * math.cos(rot_angle)

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # Item A.3: Habilitar teste de profundidade
        glEnable(GL_DEPTH_TEST)

        # Chamar configuração de iluminação:
        opengl_init_ilumination()

        glPushMatrix()
        gluLookAt(eye[0], eye[1], eye[2],
                  target[0], target[1], target[2],
                  up[0], up[1], up[2])

        cadeira.desenhar_cadeira()

        glPopMatrix()

        pygame.display.flip()
        pygame.time.wait(10)


main_cadeira()
